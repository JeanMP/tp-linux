# TP n°2 JM

- Auto-generated Table of Content
[ToC]

## :memo: 0: Prérequis

Configuration du clone db (10.55.55.12) :
```bash=
[admin@db etc]$ cat hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.55.55.13 web.tp2.cesi
10.55.55.14 rp.tp2.cesi
```

Configuration du clone web (10.55.55.13)  :
```bash=
[admin@web etc]$ cat hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.55.55.12 db.tp2.cesi
10.55.55.14 rp.tp2.cesi
```
Configuration du clone rp (10.55.55.14) :
```bash=
[admin@rp etc]$ cat hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.55.55.12 web.tp2.cesi
10.55.55.13 db.tp2.cesi
```

## :memo: I : Base de données

Installation de MariaDB (sur db.tp2.cesi) :

```bash=
Installed:
  mariadb-server.x86_64 1:5.5.68-1.el7

Dependency Installed:
  mariadb.x86_64 1:5.5.68-1.el7           perl-Compress-Raw-Bzip2.x86_64 0:2.061-3.el7   perl-Compress-Raw-Zlib.x86_64 1:2.061-4.el7
  perl-DBD-MySQL.x86_64 0:4.023-6.el7     perl-DBI.x86_64 0:1.627-4.el7                  perl-Data-Dumper.x86_64 0:2.145-3.el7
  perl-IO-Compress.noarch 0:2.061-2.el7   perl-Net-Daemon.noarch 0:0.48-5.el7            perl-PlRPC.noarch 0:0.2020-14.el7

Complete!
```

On lance le script de sécurisation de Mariadb :

```bash=
sudo mysql_secure_installation
```


Entrée dans la console Mariadb :
```bash=
$ mysql -u root -p
Enter password:
```

Création de la BDD et l'utilisateur :
```sql=
> CREATE DATABASE wordpress;
> USE wordpress;
> CREATE USER WP@10.55.55.13 IDENTIFIED BY 'bdd';
Query OK, 0 rows affected (0.00 sec)
```


Les droits sont ajoutés à l'utilisateur :
```sql=
> MariaDB [wordpress]> GRANT ALL PRIVILEGES ON wordpress.* TO WP@10.55.55.4 IDENTIFIED BY '123';
Query OK, 0 rows affected (0.00 sec)
```

Actualisation des droits :
```sql=
> FLUSH PRIVILEGES;
```

Ouverture des ports et redémarage du firewall :
```bash=
sudo firewall-cmd --permanent --zone=public --add-service=http
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --reload
sudo firewall-cmd --add-port=3306/tcp --permanent
sudo systemctl restart firewalld
```

## :memo: II : Base de données

Insaller le paquet httpd (déjà installé ici) :

```bash=
[admin@db ~]$ sudo yum install httpd
Package httpd-2.4.6-97.el7.centos.x86_64 already installed and latest version
Nothing to do
```

Récupération de Wordpress :
```bash=
[admin@db ~]$ sudo wget https://wordpress.org/latest.tar.gz
--2020-12-17 09:59:52--  https://wordpress.org/latest.tar.gz
Resolving wordpress.org (wordpress.org)... 198.143.164.252
Connecting to wordpress.org (wordpress.org)|198.143.164.252|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 15422346 (15M) [application/octet-stream]
Saving to: ‘latest.tar.gz’

100%[=============================================================================================>] 15,422,346   223KB/s   in 27s

2020-12-17 10:00:20 (560 KB/s) - ‘latest.tar.gz’ saved [15422346/15422346]
```

Extraire l'archive de WordPress :
```bash=
sudo tar -xzvf latest.tar.gz
```

Installation de dépôt additionels
```bash=
$ sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
$ sudo yum install -y yum-utils
```

On supprime d'éventuelles vieilles versions de PHP précédemment installées
```bash=
$ sudo yum remove -y php
```

Activation du nouveau dépôt
```bash=
$ sudo yum-config-manager --enable remi-php56  
```

Installation de PHP 5.6.40 et de librairies récurrentes
```bash=
$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
```

Vérification de la version installée de php :
```bash=
[admin@web /]$ php -v
PHP 5.6.40 (cli) (built: Sep 29 2020 11:13:13)
Copyright (c) 1997-2016 The PHP Group
Zend Engine v2.6.0, Copyright (c) 1998-2016 Zend Technologies
```


Configuration de Wordpress :
```bash=
[admin@web html]$ sudo cat /var/www/html/wp-config.php
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'WP' );

/** MySQL database password */
define( 'DB_PASSWORD', '123' );

/** MySQL hostname */
define( 'DB_HOST', '10.55.55.12' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
```

Démarrage d'Apache :
```bash=
[admin@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:httpd(8)
           man:apachectl(8)
[admin@web ~]$ sudo systemctl start httpd
[admin@web ~]$ sudo systemctl enable httpd
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
[admin@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-12-17 10:23:56 CET; 6s ago
     Docs: man:httpd(8)
           man:apachectl(8)
 Main PID: 2299 (httpd)
   Status: "Processing requests..."
   CGroup: /system.slice/httpd.service
           ├─2299 /usr/sbin/httpd -DFOREGROUND
           ├─2300 /usr/sbin/httpd -DFOREGROUND
           ├─2301 /usr/sbin/httpd -DFOREGROUND
           ├─2302 /usr/sbin/httpd -DFOREGROUND
           ├─2303 /usr/sbin/httpd -DFOREGROUND
           └─2304 /usr/sbin/httpd -DFOREGROUND

Dec 17 10:23:55 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 17 10:23:56 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
```

```bash=
[admin@web ~]$ sudo firewall-cmd --permanent --zone=public --add-service=http
success
[admin@web ~]$ sudo firewall-cmd --permanent --zone=public --add-service=https
success
[admin@web ~]$ sudo systemctl restart firewalld
```

## :memo: III : Reverse proxy

Installation du serveur NGINX :
```bash=
sudo yum install epel-release
sudo yum install nginx
```

Configuration d'NGINX :
```bash=
[admin@rp nginx]$ sudo cat nginx.conf
events {}

http {
        server{
                listen          80;

                server_name web.cesi;

                location / {
                        proxy_pass      http://10.55.55.13:80;
                }
        }
}
```

## :memo: IV : Un peu de sécu
### 1 : Fail2Ban

Installation de Fail2Ban et des packages nécessairess : 
```bash=
sudo yum install epel-release
sudo yum install fail2ban
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```

Modification du fichier de coinfiguration :
```bash=
sudo cat /etc/fail2ban/jail.local

[DEFAULT]
bantime = 3600

findtime = 600
maxretry = 3
action = %(action_mwl)s

[sshd]
enabled = true
```

Démarrage et activation du service fail2ban :
```bash=
sudo systemctl start fail2ban.service
sudo systemctl enable fail2ban.service
```

Vérification de fail2ban :
```bash=
sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd
```

```bash=
sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     0
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 0
   |- Total banned:     0
   `- Banned IP list:
```
