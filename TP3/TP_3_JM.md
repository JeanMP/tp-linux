# TP n°3 JM

- Auto-generated Table of Content
[ToC]

## :memo: I : DNS
### 1 : Présentation

Paramétrage :
```bash=

```

### 2 : Mise en place

Installation des packages nécessaires :
```bash=
sudo yum install epel-release
sudo yum install bind
sudo yum install bind-utils
```

Ouverture du port servant au DNS (53) :
```bash=
sudo firewall-cmd --permanent --add-service=dns
```